// Course Model
const coursesData = [
	{
		id: "wdc-001",
		name: "PHP-Laravel",
		description: "The most popular programming language",
		price: 45000,
		onOffer: true,
		start_date: '2021-02-20',
		end_date: '2021-08-19'
	},
	{
		id: "wdc-002",
		name: "Python-Django",
		description: "Multi-purpose programming language",
		price: 50000,
		onOffer: true,
		start_date: '2021-02-25',
		end_date: '2021-07-05'
	},
	{
		id: "wdc-003",
		name: "Java-Springboot",
		description: "One of the popular server-side programming language",
		price: 55000,
		onOffer: true,
		start_date: '2021-03-15',
		end_date: '2021-10-21'
	}
];

export default coursesData;