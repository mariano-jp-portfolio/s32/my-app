// Users Model
const usersData = [
	{
		email: "admin@email.com",
		password: "123",
		isAdmin: true
	},
	{
		email: "marywhite@email.com",
		password: "123",
		isAdmin: false
	}
];

export default usersData;