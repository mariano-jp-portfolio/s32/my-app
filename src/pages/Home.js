// Base import
import React, { Fragment } from 'react';

// Component imports
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

// Export Home functional component
export default function Home() {
	const data = {
		title: "Booker's",
		content: "Best part is no part, best process is no process",
		destination: "/courses",
		label: "View Courses"
	};
	
	return (
		<Fragment>
			<Banner data={data} />
			<Highlights />
		</Fragment>
	);
}