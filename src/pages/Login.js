// Base import
import React, { Fragment, useState, useEffect, useContext } from 'react';

// React-Bootstrap
import { Form, Button } from 'react-bootstrap';

// UserContext import
import UserContext from '../UserContext';

// Export Login page functional component
export default function Login() {
	// UserContext destructuring
	const { user, setUser } = useContext(UserContext);
	
	// States
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);
	const [btnColor, setBtnColor] = useState('primary');
	
	// Events
	const handleEmail = (event) => {
		setEmail(event.target.value);
	};
	
	const handlePassword = (event) => {
		setPassword(event.target.value);
	};
	
	// Effect
	useEffect(() => {
		if (email !== '' && password !== '') {
			setBtnColor('primary');
			setIsDisabled(false);
		} else {
			setBtnColor('secondary');
			setIsDisabled(true);
		}
	}, [email, password]);
	
	function authenticate(event) {
		event.preventDefault();
		
		setEmail('');
		setPassword('');
		
		// set the email of the user
		
		alert(`Hi ${email}, welcome back!`);
	}
	
	return (
		<Fragment>
			<h3>Login</h3>
			<Form onSubmit={authenticate}>
				<Form.Group controlId="formBasicEmail">
					<Form.Label>Email</Form.Label>
					<Form.Control 
						type="email"
						value={email}
						placeholder="Your email"
						onChange={handleEmail}
						required
					/>
				</Form.Group>
				
				<Form.Group controlId="formBasicPassword">
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password"
						value={password}
						placeholder="Safe word"
						onChange={handlePassword}
						required
					/>
				</Form.Group>
				
				<Button variant={btnColor} type="submit" disabled={isDisabled}>Login</Button>
			</Form>
		</Fragment>
	);
}