// Base import
import React, { Fragment, useState, useEffect } from 'react';

// React-Bootstrap
import { Form, Button } from 'react-bootstrap';

// Export Register page functional component
export default function Login() {
	// States
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);
	const [btnColor, setBtnColor] = useState('primary')
	
	// Events
	const handleEmail = (event) => {
		setEmail(event.target.value);
	};
	
	const handlePassword1 = (event) => {
		setPassword1(event.target.value);
	};
	
	const handlePassword2 = (event) => {
		setPassword2(event.target.value);
	};
	
	// Effect
	function register(event) {
		event.preventDefault();
		
		setEmail('');
		setPassword1('');
		setPassword2('');
		
		alert('Yay! You\'re now a member.')
	}
	
	useEffect(() => {
		if ((email !== '' && password1 !== '' && password2 !== '') && password2 === password1) {
			setBtnColor('primary');
			setIsDisabled(false);
		} else {
			setBtnColor('secondary');
			setIsDisabled(true);
		}
	}, [email, password1, password2]);
	
	return (
		<Fragment>
			<h3>Register</h3>
			<Form onSubmit={register}>
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email"
						value={email}
						onChange={handleEmail}
						required
					/>
					<Form.Text className="text-muted">
						We will never share your deets with anyone else.
					</Form.Text>
				</Form.Group>
				
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password" 
						placeholder="Password" 
						value={password1} 
						onChange={handlePassword1} 
						required
					/>
				</Form.Group>
				
				<Form.Group>
					<Form.Label>Verify Password</Form.Label>
					<Form.Control 
						type="password" 
						placeholder="Verify your password" 
						value={password2} 
						onChange={handlePassword2} 
						required
					/>
				</Form.Group>
				
				<Button variant={btnColor} type="submit" disabled={isDisabled} id="submitBtn">Submit</Button>
			</Form>
		</Fragment>
	);
}