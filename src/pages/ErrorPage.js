// Base import
import React from 'react';

// Banner import
import Banner from '../components/Banner';

// Export ErrorPage functional component
export default function Error() {
	const data = {
		title: "404 error",
		content: "Hey, you shouldn't be here!",
		destination: "/",
		label: "Go back Home"
	};
	
	return (
		<Banner data={data} />
	);
}