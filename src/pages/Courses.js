// Base import
import React, { Fragment } from 'react';

// Database import
import coursesData from '../data/courses';

// Course cards component import
import Course from '../components/Course';

// Export Courses page functional component
export default function Courses() {
	// Map our database
	const CourseCards = coursesData.map((course) => {
		// returns key-value pairs
		return <Course key={course.id} course={course} />
	});
	
	// returns Course prop that is to be used in Course.js component
	return (
		<Fragment>{CourseCards}</Fragment>
	);
};