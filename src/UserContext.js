// Base import
import React from 'react';

// Create a context object
const UserContext = React.createContext();

// Prop of our context object
export const UserProvider = UserContext.Provider;

export default UserContext;

/*
UserContext {
	Provider {
	UserProvider
	}
}
*/