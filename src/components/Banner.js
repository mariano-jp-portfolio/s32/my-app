// Base import
import React from 'react';
// Link
import { Link } from 'react-router-dom';

// React-Bootstrap
import { Jumbotron, Button, Row, Col } from 'react-bootstrap';

// Export Banner functional component
export default function Banner({data}) {
	const {title, content, destination, label} = data;
	
	return (
		<Row>
			<Col>
				<Jumbotron>
					<h1>{title}</h1>
					<h5>{content}</h5>
					<p>
						<Button as={Link} to={destination} variant="outline-info">{label}</Button>
					</p>
				</Jumbotron>
			</Col>
		</Row>
	);
}