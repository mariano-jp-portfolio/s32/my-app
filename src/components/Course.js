// Base import
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

// React-Bootstrap
import { Card, Button } from 'react-bootstrap';

// Export Course cards functional component
export default function Course({course}) {
	// Destructuring course parameter
	const {name, description, price} = course;
	
	// States
	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(10);
	const [isDisabled, setIsDisabled] = useState(true);
	const [btnColor, setBtnColor] = useState('success');
	
	// Hooks
	function enroll() {
		setCount(count + 1);
		setSeat(seat - 1);
	}
	
	// Effect
	useEffect(() => {	
		if (seat !== 0) {
			setBtnColor('success');
			setIsDisabled(false);
		} else {
			setBtnColor('secondary');
			setIsDisabled(true);
		}
	}, [seat]);
	
	// To be rendered
	return (
		<Card className="my-3">
			<Card.Body className="cardBG">
				<Card.Title><h2>{name}</h2></Card.Title>
				<Card.Text>
					<p className="subtitle">
						{description}
					</p>
					<p className="subtitle">
						&#8369;{price}
					</p>
				</Card.Text>
				<Card.Text>Enrollees: {count}</Card.Text>
				<Card.Text>Seats Available: {seat}</Card.Text>
				<Button onClick={enroll} disabled={isDisabled} variant={btnColor}>Enroll</Button>
			</Card.Body>
		</Card>
	);
}

// PropTypes
Course.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
};