// Base import
import React from 'react';

// React-Bootstrap
import { Row, Col, Card } from 'react-bootstrap';

// Export Highlights functional component
export default function Highlights() {
	return (
		<Row>
			<Col>
				<Card className="my-3">
					<Card.Body className="cardBG">
						<Card.Title><h2>Learn from Home</h2></Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda ut voluptatum eum sequi eveniet obcaecati praesentium officiis quia, earum. Consequatur excepturi nemo cum consectetur possimus inventore harum dolorum ea repudiandae.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			
			<Col>
				<Card className="my-3">
					<Card.Body className="cardBG">
						<Card.Title><h2>Study Now Pay Later</h2></Card.Title>
						<Card.Text>
							Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquam quidem quis modi blanditiis sit quas et porro, error nulla quaerat placeat laboriosam iusto animi temporibus illo at! Quos, blanditiis! Tempora?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			
			<Col>
				<Card className="my-3">
					<Card.Body className="cardBG">
						<Card.Title><h2>Be a Part of Our Community</h2></Card.Title>
						<Card.Text>
							Lorem ipsum, dolor sit amet, consectetur adipisicing elit. Quibusdam sunt provident expedita ducimus porro eius eaque autem vero natus nam asperiores ab delectus error quasi laboriosam aperiam, molestias saepe nihil.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	);
}