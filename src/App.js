// Base import
import React, { Fragment, useState } from 'react';
// Router
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

// App CSS
import './App.css';

// React-Bootstrap
import { Container } from 'react-bootstrap'

// Page component imports
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/ErrorPage';

// Navbar import
import NavBar from './components/Navbar';

// UserProvider import
import { UserProvider } from './UserContext';

// Export App functional component
export default function App() {
	// State hook for the user state, for the global scope
	// Initialized an object with properties from the localStorage
	const [user, setUser] = useState({
		email: localStorage.getItem('email'),
		isAdmin: localStorage.getItem('isAdmin') === true
	});
	
	// Function for clearing localStorage when user logs out
	const unsetUser = () => {
		localStorage.clear();
		
		// Changes the value of the user state back to its original value
		setUser({
			email: null,
			isAdmin: null			
		});
	};
	
	return (
		<Fragment>
			<UserProvider value={{user, setUser, unsetUser}}>
				<Router>
				<NavBar />
				<Container className="my-3">
					<Switch>
						<Route exact path="/" component={Home} />
						<Route exact path="/courses" component={Courses} />
						<Route exact path="/register" component={Register} />
						<Route exact path="/login" component={Login} />
						<Route exact path="/logout" component={Logout} />
						<Route component={ErrorPage} />
					</Switch>
				</Container>
				</Router>
			</UserProvider>
		</Fragment>
	);
}